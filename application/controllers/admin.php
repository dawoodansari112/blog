<?php 
/**
 * 
 */
class Admin extends CI_Controller
{
	
	public function index()
	{
		$this->load->view('admin/index');
	}
	public function insert()
	{
		$data=$_POST;
		if ((!isset($data['title']) || empty($data['title'])) || 
			(!isset($data['category']) || empty($data['category'])) || 
			(!isset($data['description']) || empty($data['description'])) 
		)
		{
			$array = array('status' => 'error','message'=> 'All Fields is Required.');
			echo json_encode($array);
			exit;
		}
		if ($this->do_upload() == 'error') {
			$array = array('status' => 'error','message'=> 'Image Upload Error');
			echo json_encode($array);
			exit;
		}
		else
			{
				$data['image'] = $this->do_upload();
			}

		$this->load->model('blog');
		$result=$this->blog->insert($data);
		if ($result==true) {
			$array = array('status' => 'success','message'=> 'Blog Created Successfully.');
			echo json_encode($array);
			exit;
		}
	}


	function do_upload()
	{
	    $config['upload_path'] = './assets/images/';
	    $config['allowed_types'] = 'gif|jpg|png';
	    // $config['max_size'] = '100';
	    // $config['max_width']  = '1024';
	    // $config['max_height']  = '768';
	    $config['overwrite'] = TRUE;
	    $config['encrypt_name'] = TRUE;
	    $config['remove_spaces'] = TRUE;
	    if ( ! is_dir($config['upload_path']) ) die("THE UPLOAD DIRECTORY DOES NOT EXIST");
	    $this->load->library('upload', $config);
	    if ( ! $this->upload->do_upload('image')) {
	        return 'error';
	    } else {

	        return  $this->upload->data('file_name');
	    }
	}
}