<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="<?=base_url()?>assets/frontend/img/favicon.png" type="image/png">
        <title>Opium Blog</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/bootstrap.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/vendors/linericon/style.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/vendors/animate-css/animate.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/vendors/jquery-ui/jquery-ui.css">
        <!-- main css -->
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/style.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/frontend/css/responsive.css">
    </head>
    <body>
