<?php $this->load->view('admin/common/header.php');?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="row">
    		<div class="col-md-6">
		    	<form role="form" name="blog-form" id="blog-form" onsubmit="return false;" enctype="multipart/form-data">
		            <div class="box box-primary">
		            	<div class="box-header">
                            <h3 class="box-title">Insert New Blog </h3>
                        </div>
                        	<div class="row" style="display: none" id="error">
								<div class="col-sm-6">
									<div class="alert alert-danger" id="error-show"></div>
								</div>
								<div class="col-sm-6"></div>
							</div>
							<div class="row" style="display: none" id="success">
								<div class="col-sm-6">
									<div class="alert alert-success">Blog Successsfully Inserted</div>
								</div>
								<div class="col-sm-6"></div>
							</div>	
		            <div class="box-body">
		                <div class="form-group">
		                    <label for="title">Title:</label>
		                    <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title">
		                </div>
		             <div class="form-group">
                            <label>Select Category:</label>
                            <select class="form-control" name="category" id="category">
                                <option selected="selected">Select Category</option>
                                <option>News</option>
                                <option>Sports</option>
                                <option>Technology</option>
                            </select>
                        </div>
		                <div class="form-group">
		                    <label for="description">Description:</label>
		                    <textarea class="form-control" id="description" name="description"></textarea>
		                </div>
		                <div class="form-group">
		                    <label for="image">Image:</label>
		                    <input type="file" id="image" name="image" accept="image/*">
		                   
		                </div>
		                
		            </div><!-- /.box-body -->

		            <div class="box-footer">
		                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
		            </div>
		            </div>
		      	</form>
      		</div>
      	</div>
    </section>
</aside>
<?php $this->load->view('admin/common/footer.php');?>
<script type="text/javascript">
	$(document).ready(function() {
	  $("#submit").click(function(e) {

	  	var formData = new FormData(document.getElementsByName('blog-form')[0]);
	  	$("#submit").attr('disabled',true);
		$.ajax({
		  type: "post",
		  url: "<?=base_url()?>insert",
		  data: formData,
		  processData: false,
		  contentType: false,
		  dataType: "JSON",
		  success: function(data){
		  		if (data.status == 'success') 
		  		{
		  			$('#error').hide();
		  			$('#success').show();
		  			setTimeout(function() { 
		  				window.location.href = '<?=base_url()?>admin';
				    }, 2000);
		  		}
		  		else
		  		{
		  			$("#submit").attr('disabled',false);
		  			$('#error-show').text(data.message);
		  			$('#error').show();
		  		}
		  }
		});
  	});
  });
</script>
